import pyglet
from pyglet.gl import *
from cargame.game.Car import make_car


hold = False
point = 20


def update(tx):
    global point
    if hold:
        point += 5


game_window = pyglet.window.Window(width=1024, height=720)
pyglet.clock.schedule_interval(update, 1/120.0)


@game_window.event
def on_draw():
    # print(hold)
    game_window.clear()

    glClear(GL_COLOR_BUFFER_BIT)

    glLineWidth(5)
    glBegin(GL_LINES)

    make_car(100, 100)

    glColor3f(0.0, 1.0, 0.0)
    glVertex2d(point, 300)
    glVertex2d(500, 55)

    glEnd()


@game_window.event
def on_key_press(self, symbol):
    if symbol == 512:
        print(2)

        global hold
        hold = True


@game_window.event
def on_key_release(self, symbol):
    if symbol == 512:
        print(4)

        global hold
        hold = False


pyglet.app.run()
