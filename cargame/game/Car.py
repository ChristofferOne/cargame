from pyglet.gl import *


x = 0
y = 0

front = []
rear = []
right = []
left = []


def make_car(x_pos, y_pos):
    global x
    global y

    x = x_pos
    y = y_pos

    make_vectors()
    draw()


def make_vectors():
    global front
    global rear
    global right
    global left

    front = [[x + 8, y + 12], [x - 8, y + 12]]
    rear = [[x + 8, y - 12], [x - 8, y - 12]]
    right = [[x + 8, y + 12], [x + 8, y - 12]]
    left = [[x - 8, y + 12], [x - 8, y - 12]]


def draw():
    # glClear(GL_COLOR_BUFFER_BIT)
    glColor3f(1.0, 0.0, 0.0)
    line(front)
    line(rear)
    line(right)
    line(left)


def line(vector):
    glVertex2d(vector[0][0], vector[0][1])
    glVertex2d(vector[1][0], vector[1][1])
